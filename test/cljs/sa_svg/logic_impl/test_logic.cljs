(ns sa-svg.logic-impl.test-logic
  (:require [cljs.test               :refer-macros [deftest is testing run-tests]
                                     :refer [async]]
            [sa-svg.logic-impl.logic :refer [Logic]]
            [sa-svg.logic            :refer [add-title-text find-svg-element
                                             has-sa-outer-group-element?
                                             add-outer-group-element
                                             has-sa-text-element?
                                             add-outer-group-element
                                             add-title-element
                                             import-icon-url
                                             import-icon-string
                                             add-title-text
                                             get-title-text
                                             find-sa-text-element
                                             filter-kvs]]
            [sa-svg.io-impl.io       :refer [IO]]
            [sa-svg.io               :refer [icon-exists? import-icon]]
            [clojure.core.async      :refer [take! go put! chan <! alts! timeout]]
            [hickory.render          :as hr]
            [clojure.string          :as st]))

(def no-svg-element "http://localhost:3000/resources/test/no-svg-element.svg")
(def actor          "http://localhost:3000/resources/test/actor.svg")
(def after-actor    "http://localhost:3000/resources/test/after-actor.svg")
(def circle         "http://localhost:3000/resources/test/circle.svg")
(def after-circle   "http://localhost:3000/resources/test/after-circle.svg")
(def oak-tree       "http://localhost:3000/resources/test/oaktree-mono.svg")

(def circle-string (str "<svg "
                        "xmlns:svg=\"http://www.w3.org/2000/svg\" "
                        "xmlns=\"http://www.w3.org/2000/svg\">"
                        "<ellipse "
                        "style=\"fill:#ffffff;stroke:#000000;stroke-width:2px;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.99298\" "
                        "id=\"path1537\" "
                        "cx=\"24.003242\" "
                        "cy=\"20.85918\" "
                        "rx=\"17.71895\" "
                        "ry=\"16.358004\" />"))


(def done)

(defn test-async
  "Asynchronous test awaiting ch to produce a value or close."
  [ch]
  (async done
   (take! ch (fn [_] (done)))))

(deftest find-svg-no-svg-element
  (testing "Find SVG element in no svg element file"
    (let [icon-chan (chan)]
      (import-icon IO icon-chan no-svg-element)
      (test-async
        (go
          (let [icon (<! icon-chan)]
            (is (= false (find-svg-element Logic icon)))))))))

(deftest find-svg-actor
  (testing "Find SVG element in actor"
    (let [icon-chan (chan)]
      (import-icon IO icon-chan actor)
      (test-async
        (go
          (let [icon (<! icon-chan)]
            (not= false (find-svg-element Logic icon))))))))

(deftest find-svg-after-actor
  (testing "Find SVG element in modified actor"
    (let [icon-chan (chan)]
      (import-icon IO icon-chan after-actor)
      (test-async
        (go
          (let [icon (<! icon-chan)]
            (is (not= false (find-svg-element Logic icon)))))))))

(deftest find-svg-oak-tree
  (testing "Find SVG element in complex svg"
    (let [icon-chan (chan)]
      (import-icon IO icon-chan oak-tree)
      (test-async
        (go
          (let [icon (<! icon-chan)]
            (is (not= false (find-svg-element Logic icon)))))))))

(deftest svg-oak-has-outer-group
  (testing "The svg oak should not have an outer group until added"
    (let [icon-chan (chan)]
      (import-icon IO icon-chan oak-tree)
      (test-async
        (go
          (let [icon (<! icon-chan)]
            (is (false? (has-sa-outer-group-element? Logic icon)))
            (is (true? (has-sa-outer-group-element? Logic (add-outer-group-element Logic (find-svg-element Logic icon)))))))))))

(defn debug [x]
  (println (str "\nDEBUG LOGIC TEST HTML\n" (hr/hickory-to-html x) "\n\n"))
  x)

(defn debug1 [x]
  (println (str "\nDEBUG LOGIC TEST HICKORY\n" x "\n\n"))
  x)

(deftest svg-oak-has-sa-text-element
  (testing "The svg oak should not have a sa text  but should after add"
    (let [icon-chan (chan)]
      (import-icon IO icon-chan actor);oak-tree)
      (test-async
        (go
          (let [icon (<! icon-chan)]
            (is (false? (has-sa-text-element? Logic icon)))
            (is (true?  (has-sa-text-element? Logic (add-outer-group-element Logic icon))))))))))

(deftest import-actor-icon
  (testing "Import a simple icon"
    (let [icon-chan (chan)]
      (import-icon-url Logic icon-chan actor)
      (test-async
        (go
          (let [icon (<! icon-chan)]
            (is (not= false icon))
            (is (true? (has-sa-text-element? Logic icon)))))))))


(deftest find-sa-text-element-oak-icon
  (testing "Import oak tree icon"
    (let [icon-chan (chan)]
      (import-icon-url Logic icon-chan oak-tree)
      (test-async
        (go
          (let [icon (<! icon-chan)]
            (is (not= false icon))
            (is (true? (has-sa-text-element? Logic icon)))))))))

(deftest get-default-title
  (testing "Import a simple icon with no text field"
    (let [icon-chan (chan)]
      (import-icon-url Logic icon-chan actor)
      (test-async
        (go
          (let [icon (<! icon-chan)
                title-text     (get-title-text Logic icon)]
            (is (= true (has-sa-text-element? Logic icon)))
            (is (= nil title-text))))))))


(deftest add-title
  (testing "Add title to imported actor"
    (let [icon-chan (chan)]
      (import-icon-url Logic icon-chan circle) ;actor)
      (test-async
        (go
          (let [icon (<! icon-chan)
                icon-with-text (add-title-text Logic "New Title" icon)
                title-text     (get-title-text Logic icon-with-text)]
            (is (true? (has-sa-text-element? Logic icon)))
            (is (= "New Title" title-text))))))))

(deftest add-title-oak-icon
  (testing "Add title to oak icon"
    (let [icon-chan (chan)]
      (import-icon-url Logic icon-chan oak-tree)
      (test-async
        (go
          (let [icon (<! icon-chan)
                icon-with-text (add-title-text Logic "New Oak Tree Title" icon)
                title-text     (get-title-text Logic icon-with-text)]
            (is (= "New Oak Tree Title" title-text))))))))


(deftest change-title-oak-icon
  (testing "Change title to oak icon"
    (let [icon-chan (chan)]
      (import-icon-url Logic icon-chan oak-tree)
      (test-async
        (go
          (let [icon (<! icon-chan)
                icon-with-text (add-title-text Logic "New Oak Tree Title" icon)
                title-text     (get-title-text Logic icon-with-text)
                icon-with-new-title-text (add-title-text Logic "Changed Oak Tree Title" icon-with-text)
                new-title-text (get-title-text Logic icon-with-new-title-text)]
            (is (= "New Oak Tree Title" title-text))
            (is (= "Changed Oak Tree Title" new-title-text))))))))

(deftest test-complexkvs-filter
  (testing "Test we can clean up a simple svg file"
    (let [icon-chan (chan)]
      (import-icon-url Logic icon-chan oak-tree)
      (test-async
        (go
          (let [icon (<! icon-chan)
                clean-icon (filter-kvs Logic icon)]
            (is (false? (st/includes? (str (first clean-icon)) "xmlns:dc")))))))))


(deftest test-import-icon-from-string
  (testing "Test we can import a cicle from an svg string"
    (let [imported-circle (import-icon-string Logic circle-string)]
      (is (= true (has-sa-text-element? Logic imported-circle))))))
