(ns sa-svg.io-impl.test-io
  (:require [cljs.test            :refer-macros [deftest is testing run-tests]
                                  :refer [async]]
            [sa-svg.io-impl.io    :refer [IO]]
            [sa-svg.io            :refer [icon-exists? import-icon]]
            [clojure.core.async   :refer [take! go put! chan <! alts! timeout]]))

(def t 2000)

(def no-file      "http://sa.softwarebynumbers.com:3000/Nope")
(def actor        "http://localhost:3000/resources/test/actor.svg")
(def after-actor  "http://localhost:3000/resources/test/after-actor.svg")
(def circle       "http://localhost:3000/resources/test/circle.svg")
(def after-circle "http://localhost:3000/resources/test/after-circle.svg")
(def oak-tree     "http://localhost:3000/resources/test/oaktree-mono.svg")
(def remote       "https://upload.wikimedia.org/wikipedia/commons/1/1e/SVG_icon_%28letters%29.svg")

(def done)

(defn test-async
  "Asynchronous test awaiting ch to produce a value or close."
  [ch]
  (async done
   (take! ch (fn [_] (done)))))

(deftest io-exists
  (testing "exists"
    (let [c (chan)]
      (icon-exists? IO c actor)
      (test-async
        (go (is (true? (<! c))))))))

(deftest io-not-exists
  (testing "does not exists"
    (let [c (chan)]
      (icon-exists? IO c no-file)
      (test-async
        (go (is (false? (<! c))))))))

(deftest good-read-actor-svg
  (testing "good read simple"
    (let [c (chan)]
      (import-icon IO c remote)
      (test-async
        (go (is (not= nil (<! c))))))))

(deftest good-read-circle-svg
  (testing "good read simple"
    (let [c (chan)]
      (import-icon IO c circle)
      (test-async
        (go (is (not= nil (<! c))))))))

(deftest good-read-complex-svg
  (testing "good read complex"
    (let [c (chan)]
      (import-icon IO c oak-tree)
      (test-async
        (go (is (not= nil (<! c))))))))

(deftest bad-read-sv
  (testing "bad read"
    (let [c (chan)]
      (import-icon IO c no-file)
      (test-async
        (go (is (= false (<! c))))))))

(deftest read-remote-svg
  (testing "good read complex"
    (let [c (chan)]
      (import-icon IO c remote)
      (test-async
        (go
          (let [icon (<! c)]
            (is (not= nil icon))
            (is (not= false icon))))))))
