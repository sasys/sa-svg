(ns sa-svg.test-runner
  (:require [cljs.test               :refer-macros [run-tests]]
            [sa-svg.logic-impl.logic]))

(defn trun []
  (println (str "\n\nRUNNING TESTS FOR CLJS\n=====================\n\n"))
  (run-tests 'sa-svg.logic-impl.logic))
