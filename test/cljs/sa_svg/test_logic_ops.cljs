(ns sa-svg.test-logic-ops
  (:require [cljs.test               :refer-macros [deftest is testing run-tests]
                                     :refer [async]]
            [sa-svg.logic-ops :refer [add-sa-text-to-node
                                      add-outer-group-element
                                      add-sa-outer-group-to-node
                                      has-sa-outer-group-element?
                                      find-sa-outer-group
                                      find-sa-text-element
                                      has-sa-text-element?
                                      add-title-text
                                      get-title
                                      get-transform-string
                                      get-group-transform
                                      translate-x
                                      translate-y
                                      scale-x
                                      scale-y
                                      make-transform-string
                                      add-transform-string-to-node
                                      update-group-transform
                                      filter-kvs
                                      edit-remove-attrs
                                      react-style->hiccup
                                      find-svg-element
                                      has-svg-element
                                      add-svg-element
                                      has-view-box
                                      get-view-box
                                      add-view-box
                                      update-viewbox-x
                                      update-viewbox-y
                                      update-viewbox-width
                                      update-viewbox-height
                                      add-viewport
                                      get-viewport
                                      has-viewport?]]
            [hickory.core      :refer [parse
                                       parse-fragment
                                       as-hiccup
                                       as-hickory]]
            [hickory.render          :refer [hickory-to-html]]
            [hickory.zip             :as hz   :refer [hickory-zip]]
            [hickory.convert         :as hicon]
            [sa-svg.logic-impl.logic :refer [Logic]]
            [clojure.core.async      :refer [take! go put! chan <! alts! timeout]]
            [sa-svg.logic            :as logic]
            [reagent.core            :as r]))


;A remote sample
(def oak-tree "http://localhost:3000/resources/test/oaktree-mono.svg")



;A sample circle before ingestion
(def circle (str "<svg "
                 "xmlns:svg=\"http://www.w3.org/2000/svg\" "
                 "xmlns=\"http://www.w3.org/2000/svg\">"
                 "<ellipse "
                 "style=\"fill:#ffffff;stroke:#000000;stroke-width:2px;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.99298\" "
                 "id=\"path1537\" "
                 "cx=\"24.003242\" "
                 "cy=\"20.85918\" "
                 "rx=\"17.71895\" "
                 "ry=\"16.358004\" />"))

;A simple circle with viewport to test pan and zoom
(def circle1 (str "<svg width=\"100\"
                        height=\"100\"
                        id=\"svg-circ-1\" xmlns=\"http://www.w3.org/2000/svg\" >"
                    "<ellipse cx=\"20\" cy=\"20\" rx=\"20\" ry=\"20\" />"
                    "</svg>"))


;A simple circle with viewport and viewbox to test pan and zoom
(def circle2 (str "<svg width=\"100\"
                        height=\"100\"
                        viewBox=\"0 0 40 40\"
                        id=\"svg-circ-2\" xmlns=\"http://www.w3.org/2000/svg\" >"
                    "<ellipse cx=\"20\" cy=\"20\" rx=\"20\" ry=\"20\" />"
                    "</svg>"))

;A sample circle after ingestion
(def decorated-circle (str "<svg "
                           "xmlns:svg=\"http://www.w3.org/2000/svg\" "
                           "xmlns=\"http://www.w3.org/2000/svg\">"
                           "<g sa:id=\"sa:icon-group\" transform=\"translate(0, 0)\" "
                           "id=\"layer1\">"
                           "<g sa:id=\"sa:st-icon-group\" "
                           "transform=\"translate(0, 0) scale(2.5, 2.5)\">"
                           "<ellipse "
                           "style=\"fill:#ffffff;stroke:#000000;stroke-width:2px;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.99298\" "
                           "id=\"path1537\" "
                           "cx=\"24.003242\" "
                           "cy=\"20.85918\" "
                           "rx=\"17.71895\" "
                           "ry=\"16.358004\">"
                           "</ellipse>"
                           "</g>"
                           "<text class=\"sa:title icon local sbnl pod\" "
                           "sa:id=\"sa:title\" "
                           "id=\"title\" "
                           "y=\"26.794041\" "
                           "x=\"7\" "
                           "text-anchor=\"middle\" "
                           "xml:space=\"preserve\" "
                           "font-size=\"4px\">"
                           "</text>"
                           "</g>"
                           "</svg>"))


;circle with no svg element
(def circle-no-svg (str "<ellipse cx=\"20\" cy=\"20\" rx=\"20\" ry=\"20\" />"))


;A circle before ingestion with messy attributes
(def messy-circle (str "<svg "
                        "xmlns:svg=\"http://www.w3.org/2000/svg\" "
                        "xmlns=\"http://www.w3.org/2000/svg\">"
                        "<ellipse "
                        "inkscape:groupmode=\"A\" "
                        "inkscape:label=\"B\" "
                        "xmlns:inkscape=\"C\" "
                        "style=\"fill:#ffffff;stroke:#000000;stroke-width:2px;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.99298\" "
                        "id=\"path1537\" "
                        "cx=\"24.003242\" "
                        "cy=\"20.85918\" "
                        "rx=\"17.71895\" "
                        "ry=\"16.358004\" />"
                        "</elipse>"
                        "</svg>"))

(def done)

(defn test-async
 "Asynchronous test awaiting ch to produce a value or close."
 [ch]
 (async done
  (take! ch (fn [_] (done)))))

(deftest test-add-sa-text-to-text-element
  (testing "We can add some text to a node that may or may not have a text element"
    (let [node {:content ["none"]}
          nnode1 (add-sa-text-to-node [node] "New Text")
          nnode2 (add-sa-text-to-node node "New Text")
          nnode3 (add-sa-text-to-node [node])
          nnode4 (add-sa-text-to-node node)]
      (is (= (-> (first nnode1)
                 :content) "New Text"))
      (is (= (-> nnode2
                 :content) "New Text"))
      (is (= (-> (first nnode3)
                 :content) ""))
      (is (= (-> nnode4
                 :content) "")))))


(deftest test-add-outer-group-element
  (testing "Add an outer group element to an svg"
    (let [hickory-circ (-> circle
                           parse
                           as-hickory)
          circ (add-sa-outer-group-to-node hickory-circ)]
      (is (not= false circ))
      (is (= true (has-sa-outer-group-element? circ))))))


(deftest test-find-sa-outer-group
  (testing ""
    (let [hickory-circ (-> circle
                           parse
                           as-hickory)
          circ (add-sa-outer-group-to-node hickory-circ)]
      (is (not= false (find-sa-outer-group circ))))))


(deftest test-find-sa-text-element
  (testing "Has an svg got an sa text element"
    (let [hickory-circ (-> circle
                           parse
                           as-hickory)
          circ (add-sa-outer-group-to-node hickory-circ)]
      (is (not= false (find-sa-text-element circ))))))


(deftest test-has-sa-text-element
  (testing "Has an svg got an sa text element"
    (let [hickory-circ (-> circle
                           parse
                           as-hickory)
          circ (add-sa-outer-group-to-node hickory-circ)]
      (is (= true (has-sa-text-element? circ))))))

(deftest add-title-text-to-sa-node
  (testing "Add text element to node"
    (let [hickory-circ (-> circle
                           parse-fragment
                           first
                           as-hickory)
          circ  (add-sa-outer-group-to-node hickory-circ)
          tcirc (add-title-text "New Text" circ)]
      (is (not= false (find-sa-text-element tcirc)))
      (is (= true (has-sa-text-element? tcirc)))
      (is (= "New Text" (get-title tcirc)))
      (let [newcirc (add-title-text "Other Text" circ)]
        (is (= "Other Text" (get-title newcirc)))))))


;A more complex svg
(deftest add-title-text-to-oaktree
  (testing "Test we can add a title to a more complex svg"
    (let [icon-chan (chan)]
      (logic/import-icon-url Logic icon-chan oak-tree)
      (test-async
        (go
          (let [oak-tree-icon (<! icon-chan)
                toak-tree (add-title-text "My Oak Tree" oak-tree-icon)]
            (is (not= false (find-sa-text-element toak-tree)))
            (is (= true (has-sa-text-element? toak-tree)))
            (is (= "My Oak Tree" (get-title toak-tree)))
            (let [new-oak-tree (add-title-text "New Oak Tree Title" toak-tree)]
              (is (= "New Oak Tree Title" (get-title new-oak-tree))))))))))


(deftest test-make-transform-string
  (testing "Test we can make a transform string"
    (is (= "translate(10.5, 0) scale(0, 0)" (make-transform-string [[10.5 0] [0 0]])))))


(deftest test-get-group-transform
  (testing "Test we can get the transform from an svg as destructure translate
            and scale pairs."
    (let [hickory-circ (-> circle
                           parse-fragment
                           first
                           as-hickory)
          circ (add-outer-group-element hickory-circ)]
      (is (not= false (find-sa-text-element circ)))
      (let  [trans-str (get-transform-string circ)
             trans     (get-group-transform trans-str)]
        (is (not= false trans-str))
        (is (= "translate(0, 0) scale(2.5, 2.5)" trans-str))
        (is (not= false trans))
        (is (= [[0 0] [2.5 2.5]] trans))
        (let [[[trans-x _] [_ scale-y]] trans]
          (is (= trans-x 0))
          (is (= scale-y 2.5)))))))


(def sa-st-icon-group-1
  {:type :element,
   :attrs {:sa:id "sa:st-icon-group",
           :transform "translate(0, 0) scale(2.5, 2.5)"},
   :tag :g,
   :content []})

(def sa-st-icon-group
  "<g sa:id=\"sa:st-icon-group\"
      transform=\"translate(0, 0) scale(2.5, 2.5)\"/>")


(deftest test-add-transform-string-to-node
  (testing "Test we can add a new transform string"
    (let [hickory-gp (-> sa-st-icon-group
                         parse-fragment
                         first
                         as-hickory)]
      (is (= {:type :element,
              :attrs {:sa:id "sa:st-icon-group", :transform "translate(33,33) scale(99, 99)"}
              :tag :g
              :content nil}
             (add-transform-string-to-node hickory-gp "translate(33,33) scale(99, 99)"))))))


(deftest test-update-group-transform
 (testing "Test we can update the transform on an svg."
   (let [hickory-circ (-> circle
                          parse-fragment
                          first
                          as-hickory)
         circ (add-outer-group-element hickory-circ)]
      (is (not= false (find-sa-text-element circ)))
      (let  [trans-str (get-transform-string circ)
             trans     (get-group-transform trans-str)
             [[trans-x _] [_ scale-y]] trans]

        (is (= trans-x 0))
        (is (= scale-y 2.5))
        ;Now update the circle transform string
        (let [updated (update-group-transform circ [[19 18.6][17.6 15]])
              trans-str (get-transform-string updated)
              trans     (get-group-transform trans-str)
              [[trans-x trans-y] [scale-x scale-y]] trans]
          (is (= trans-x 19))
          (is (= trans-y 18.6))
          (is (= scale-x 17.6))
          (is (= scale-y 15)))))))



;A more complex svg
(deftest test-change-scale-of-oaktree
  (testing "Test we can change the scale of a more complex svg"
    (let [icon-chan (chan)]
      (logic/import-icon-url Logic icon-chan oak-tree)
      (test-async
        (go
          (let [oak-tree-icon (<! icon-chan)
                t-oak-tree (add-title-text "My Oak Tree" oak-tree-icon)]
            (is (not= false (find-sa-text-element t-oak-tree)))
            (let  [trans-str (get-transform-string t-oak-tree)
                   trans     (get-group-transform trans-str)
                   [[trans-x _] [_ scale-y]] trans]

              (is (= trans-x 0))
              (is (= scale-y 2.5))
              (let [updated (update-group-transform t-oak-tree [[19 18.6][17.6 15]])
                    trans-str (get-transform-string updated)
                    trans     (get-group-transform trans-str)
                    [[trans-x trans-y] [scale-x scale-y]] trans]
                (is (= trans-x 19))
                (is (= trans-y 18.6))
                (is (= scale-x 17.6))
                (is (= scale-y 15))))))))))


(deftest test-circle-complete-import
 (testing "Test we can import a circle"
   (let [hickory-circ (-> circle
                          parse-fragment
                          first
                          as-hickory)
         circ (add-outer-group-element hickory-circ)]
    (println (hickory-to-html circ))
    (println "\n\n")
    (println decorated-circle)
    (is (= (hickory-to-html circ) decorated-circle)))))


(deftest test-edit-remove-attrs
  (testing "Test we can remove a list of attributes froma hickory zipper node"
    (let [unwanted-keys [:inkscape:label
                         :inkscape:groupmode]
          node          {:type :element
                         :attrs {:cy 20
                                 :inkscape:label "B"}
                         :tag "svg"
                         :content []}
          cleaned (edit-remove-attrs node unwanted-keys)]
      (is (= cleaned {:type :element
                      :attrs {:cy 20}
                      :tag "svg"
                      :content []})))))

(deftest test-simplekvs-filter
  (testing "Test we can clean up a simple svg file"
    (let [unwanted-keys [:inkscape:groupmode
                         :inkscape:label
                         :xmlns:inkscape]
          hickory-messy-circ (-> messy-circle
                                 parse-fragment
                                 first
                                 as-hickory
                                 hz/hickory-zip)
          hickory-clean-circ (-> circle
                                 parse-fragment
                                 first
                                 as-hickory
                                 hz/hickory-zip)
          clean-circ (-> hickory-messy-circ
                         (filter-kvs edit-remove-attrs unwanted-keys))]
      (is (= (first clean-circ) (first hickory-clean-circ))))))


(deftest test-react-style-to-hiccup
  (testing "Test we can convert react style string to a map"
    (let [original-1 {:style "a:b;c:d"}
          expected-1  {:style {"a" "b" "c" "d"}}]
      (is (= expected-1 (react-style->hiccup original-1))))))


; (deftest back-to-hiccup
;   (testing "Can we round trip a circle"
;     (let [hickory-circ (-> circle
;                            parse
;                            as-hickory)]
;       (is (= circle (hicon/hickory-to-hiccup (-> hickory-circ
;                                                  :content
;                                                  first
;                                                  react-style->hiccup)))))))


(deftest find-svg-box-with-viewport
  (testing "Can we find an svg bounding box where a viewport is present"
    (let [hiccup-circ  (-> circle1
                           parse
                           as-hiccup
                           react-style->hiccup)
          div (.getElementById js/document "target1")]
      (r/render hiccup-circ div)
      (let [svg-circ    (.getElementById js/document "svg-circ-1")
            box         (.getBoundingClientRect svg-circ)
            width       (.-width box)
            height      (.-height box)]
        (is (= 100 width))
        (is (= 100 height))))))


(deftest find-svg-box-with-viewport-and-box
  (testing "Can we find an svg bounding box where a viewport and view box are present"
    (let [hiccup-circ  (-> circle2
                           parse
                           as-hiccup
                           react-style->hiccup)
          div (.getElementById js/document "target1")]
      (r/render hiccup-circ div)
      (let [svg-circ    (.getElementById js/document "svg-circ-2")
            box         (.getBoundingClientRect svg-circ)
            width       (.-width box)
            height      (.-height box)]
        (is (= 100 width))
        (is (= 100 height))))))



(deftest test-find-svg-element
  (testing "We can find the svg element"
    (let [hiccup-circ-no-svg  (-> circle-no-svg
                                  parse
                                  as-hickory)

          hiccup-circ-with-svg  (-> circle2
                                    parse
                                    as-hickory)]

      (is (false? (find-svg-element hiccup-circ-no-svg)))
      (is (false? (has-svg-element hiccup-circ-no-svg)))
      (is (not (false? (find-svg-element hiccup-circ-with-svg))))
      (is (not (false? (has-svg-element hiccup-circ-with-svg)))))))


(deftest test-add-svg-element
  (testing "Test we can add an svg element to a node that does not have one"
    (let [hiccup-circ-no-svg  (-> circle-no-svg
                                  parse
                                  as-hickory)]
      (is (false? (has-svg-element hiccup-circ-no-svg)))
      (let [updated-svg (add-svg-element hiccup-circ-no-svg)]
        (is (not (false? (has-svg-element updated-svg))))))))


(deftest test-has-view-box
  (testing "Test we can check that an icon has a view box"
    (let [hickory-circ-no-view-box  (-> circle1
                                        parse
                                        as-hickory)
          hickory-circ-with-view-box  (-> circle2
                                          parse
                                          as-hickory)]
      (is (false? (has-view-box hickory-circ-no-view-box)))
      (is (true? (has-view-box hickory-circ-with-view-box))))))


(deftest test-add-view-box
  (testing "Test we can add and update a view box to the icon"
    (let [hickory-circ-no-view-box  (-> circle1
                                        parse
                                        as-hickory)]
      (is (false? (has-view-box hickory-circ-no-view-box)))
      (let [view-box [0 0 100 100]
            new-view-box [20 20 300 300]
            hickory-with-view-box (add-view-box hickory-circ-no-view-box view-box)]
        (is (true? (has-view-box hickory-with-view-box)))
        (is (= view-box (get-view-box hickory-with-view-box)))
        ;;Update the view box
        (let [updated-svg (add-view-box hickory-with-view-box new-view-box)]
          (is (= (get-view-box updated-svg) new-view-box)))))))

(deftest test-update-viewbox
  (testing "We can update the x, y, width and height of a viewbox")
  (let [hickory-circ-no-view-box  (-> circle1
                                      parse
                                      as-hickory)]
    (is (false? (has-view-box hickory-circ-no-view-box)))
    (let [view-box [0 0 100 100]
          hickory-with-view-box (add-view-box hickory-circ-no-view-box view-box)
          new-svg (-> hickory-with-view-box
                      (update-viewbox-x       99)
                      (update-viewbox-y      100)
                      (update-viewbox-width  101)
                      (update-viewbox-height 102))]
      (is (= [99 100 101 102] (get-view-box new-svg))))))


(deftest test-add-viewport
  (testing "Test we can add a view port"
    (let [hickory-circ-no-viewport  (-> circle1
                                        parse
                                        as-hickory)
          width  "100"
          height "100"
          hickory-circ-with-viewport (add-viewport hickory-circ-no-viewport [width height])]
      (is (false? (has-viewport? hickory-circ-no-viewport)))
      (is (true?  (has-viewport? hickory-circ-with-viewport)))
      (is (= [width height] (get-viewport hickory-circ-with-viewport))))))


(deftest test-viewbox-resize
  (testing "Test we can change the viewbox on a complex element"
    (let [icon-chan (chan)
          view-box      [0 0 100 100]
          width  "100"
          height "100"]
      (logic/import-icon-url Logic icon-chan oak-tree)
      (test-async
        (go
          (let [oak-tree-icon (<! icon-chan)
                oak-tree-icon-with-view-box  (add-viewport (add-view-box oak-tree-icon view-box) [width height])
                div1          (. js/document (getElementById "target1"))
                div2          (. js/document (getElementById "target2"))]
            ;Add the old and new svg to the two target divs
            (r/render (hicon/hickory-to-hiccup oak-tree-icon) div1)
            (r/render (hicon/hickory-to-hiccup oak-tree-icon-with-view-box) div2)

            ;TODO Remove this sanity check!!
            ;(is (= "a" "b"))

            (let [div1              (. js/document (getElementById "target1"))
                  div2              (. js/document (getElementById "target2"))
                  svg-oak-tree1-svg (.-innerHTML div1)
                  svg-oak-tree2-svg (.-innerHTML div2)
                  svg-oak-tree1     (-> svg-oak-tree1-svg
                                        parse-fragment
                                        first
                                        as-hickory)
                  svg-oak-tree2     (-> svg-oak-tree2-svg
                                        parse-fragment
                                        first
                                        as-hickory)
                  box1             (.getBoundingClientRect div1)
                  box2             (.getBoundingClientRect div2)
                  width1           (.-width box1)
                  height1          (.-height box1)
                  width2           (.-width box2)
                  height2          (.-height box2)]
              ;(println (str svg-oak-tree2))
              (is (true? (has-view-box svg-oak-tree1)))
              (is (= 784 width1))
              (is (= 150 height1))
              (is (true? (has-view-box svg-oak-tree2)))
              ;TODO NOT WORKING
              ;(is (= 100 width2))
              (is (= 100 height2)))))))))
              ;TODO Remove this sanity check!!
              ;(is (= "c" "d")))))))))
