(ns sa-svg.logic-impl.test-logic
  (:require [clojure.test            :refer [deftest testing is]]
            [clojure.zip             :as cz]
            [hickory.zip             :as hz]
            [hickory.select          :as hs]
            [hickory.zip             :as hz]
            [hickory.core            :as hc :refer [parse parse-fragment as-hiccup as-hickory]]
            [sa-svg.io               :refer [import-icon]]
            [sa-svg.io-impl.io       :as svgio]
            [sa-svg.logic            :as svgl]
            [sa-svg.logic-impl.logic :as lg]
            [clojure.pprint          :as pp]
            [clojure.zip             :as zip]
            [clojure.string          :as st]))

(def actor          "resources/test/actor.svg")
(def after-actor    "resources/test/after-actor.svg")
(def circle         "resources/test/circle.svg")
(def after-circle   "resources/test/after-circle.svg")
(def no-svg-element "resources/test/no-svg-element.svg")


(deftest find-svg-element
  (testing "svg element"
    (let [circ-before (as-hickory (import-icon svgio/SVGIO circle))
          circ-after  (as-hickory (import-icon svgio/SVGIO after-circle))
          actor       (as-hickory (import-icon svgio/SVGIO actor))
          after-actor (as-hickory (import-icon svgio/SVGIO after-actor))]
      (is (not (nil? circ-before)))
      (is (not (nil? circ-after)))
      (is (= true (svgl/is-svg-element? lg/LogicOperations circ-before)) "We should find an svg element")
      (is (not (nil? actor)))
      (is (not (nil? after-actor)))
      (is (= true (svgl/is-svg-element? lg/LogicOperations actor)) "We should find an svg element")
      (is (= true (svgl/is-svg-element? lg/LogicOperations after-actor)) "We should find an svg element"))))

(deftest logic-ops-no
  (testing "no svg element"
    (let [no-svg (as-hickory (import-icon svgio/SVGIO no-svg-element))]
      (is (false? (svgl/find-svg-element lg/LogicOperations no-svg)))
      (is (false? (svgl/is-svg-element? lg/LogicOperations no-svg))))))


(deftest outer-group
  (testing "outer group"
    (let [circ-before  (svgl/find-svg-element lg/LogicOperations (as-hickory (import-icon svgio/SVGIO circle)))
          circ-after   (svgl/find-svg-element lg/LogicOperations (as-hickory (import-icon svgio/SVGIO after-circle)))
          actor-before (svgl/find-svg-element lg/LogicOperations (as-hickory (import-icon svgio/SVGIO actor)))
          actor-after  (svgl/find-svg-element lg/LogicOperations (as-hickory (import-icon svgio/SVGIO after-actor)))
          no-svg       (svgl/find-svg-element lg/LogicOperations (as-hickory (import-icon svgio/SVGIO no-svg-element)))]

      ;find outer group tests
      (is (false? (svgl/find-sa-outer-group lg/LogicOperations circ-before))         "Should not have an outer group")
      (is (map?   (svgl/find-sa-outer-group lg/LogicOperations circ-after))          "Should find outer group")
      (is (false? (svgl/find-sa-outer-group lg/LogicOperations actor-before))        "Should not have an outer group")
      (is (map?   (svgl/find-sa-outer-group lg/LogicOperations actor-after))         "Should find outer group")
      (is (false? (svgl/find-sa-outer-group lg/LogicOperations no-svg))              "Should not have an outer group")

      ;add outer group tests on a circle
      (is (false? (svgl/has-sa-outer-group-element? lg/LogicOperations circ-before)) "Should not have an outer group")
      (is (true?  (svgl/has-sa-outer-group-element? lg/LogicOperations circ-after))  "Should have an outer group")
      (is (true? (->> circ-before
                      (svgl/add-outer-group-element lg/LogicOperations)
                      (svgl/find-svg-element lg/LogicOperations)
                      (svgl/has-sa-outer-group-element? lg/LogicOperations)))        "Did we add the outer group element")

      ;add outer group tests on an actor
      (is (false? (svgl/has-sa-outer-group-element? lg/LogicOperations actor-before)) "Should not have an outer group")
      (is (true?  (svgl/has-sa-outer-group-element? lg/LogicOperations actor-after))  "Should have an outer group")
      (is (true? (->> actor-before
                      (svgl/add-outer-group-element lg/LogicOperations)
                      (svgl/find-svg-element lg/LogicOperations)
                      (svgl/has-sa-outer-group-element? lg/LogicOperations)))        "Did we add the outer group element"))))

(deftest title-element
  (testing "Check we have added the title to the actor and the circle"
    (let [circ-before  (svgl/find-svg-element lg/LogicOperations (as-hickory (import-icon svgio/SVGIO circle)))
          circ-after   (svgl/find-svg-element lg/LogicOperations (as-hickory (import-icon svgio/SVGIO after-circle)))
          actor-before (svgl/find-svg-element lg/LogicOperations (as-hickory (import-icon svgio/SVGIO actor)))
          actor-after  (svgl/find-svg-element lg/LogicOperations (as-hickory (import-icon svgio/SVGIO after-actor)))]
      (is (false? (svgl/has-sa-text-element? lg/LogicOperations circ-before))    "No text element before fix")
      (is (true?  (svgl/has-sa-text-element? lg/LogicOperations circ-after))     "Find text element after fix")
      (is (true?  (->> circ-before
                       (svgl/add-outer-group-element lg/LogicOperations)
                       (svgl/find-svg-element lg/LogicOperations)
                       (svgl/has-sa-text-element? lg/LogicOperations))) "Did we add the title text element to the circle?")

      (is (false? (svgl/has-sa-text-element? lg/LogicOperations actor-before))   "No text element before fix")
      (is (true?  (svgl/has-sa-text-element? lg/LogicOperations actor-after))    "Find text element after fix")
      (is (true?  (->> actor-before
                       (svgl/add-outer-group-element lg/LogicOperations)
                       (svgl/find-svg-element lg/LogicOperations)
                       (svgl/has-sa-text-element? lg/LogicOperations))) "Did we add the title text element to the actor?"))))
