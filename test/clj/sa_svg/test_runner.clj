(ns sa-svg.test-runner
  (:require [clojure.test               :refer [run-tests]]
            [sa-svg.logic-impl.test-logic]
            [sa-svg.io-impl.test-io]))


(defn trun
  ([]
   (trun ""))
  ([_]
   (println (str "\n\nRUNNING TESTS FOR CLJ\n=====================\n\n"))
   (run-tests 'sa-svg.logic-impl.test-logic
              'sa-svg.io-impl.test-io)))
