(ns sa-svg.io-impl.test-io
  (:require [clojure.test      :refer [deftest testing is]]
            [sa-svg.io         :refer [icon-exists? import-icon]]
            [sa-svg.io-impl.io :as svgio]))

(def actor "resources/test/actor.svg")

(deftest io-ops

  (testing "IO exists."
    (is (= (icon-exists? svgio/SVGIO actor) true))
    (is (= (icon-exists? svgio/SVGIO "No-not-me") false)))

  (testing "IO import"
    (is (not (nil? (import-icon svgio/SVGIO actor))))))
