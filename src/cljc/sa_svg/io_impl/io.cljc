(ns sa-svg.io-impl.io
  "
    Provide a clojure implementation of the IO SVG protocol.
    This implementation uses files for io, ie the files must be available
    on the file system somewhere.
  "
  (:require        [sa-svg.io                    :refer [SVGIOOperations icon-exists?]]
                   #?(:clj  [clojure.java.io     :as io])
                   #?(:cljs [cljs-http.client    :as client])
                   #?(:cljs [clojure.core.async  :refer [go put! chan]])
                   [hickory.core                 :refer [parse parse-fragment as-hiccup as-hickory]]
                   #?(:cljs [ajax.core           :refer [GET ajax-request]])))

(defn debug [x]
  (println (str "\nDEBUG IO\n" x "\n\n"))
  x)


#?(:clj (def SVGIO
          (reify SVGIOOperations

            (icon-exists?
              [this path]
              (.exists (io/file path)))

            (import-icon
              [this path]
              "
                Import an icon from the given path with the given protocol.
                ie http://path.svg or file:///path.svg  Returns nil if the icon canot be
                loaded.  The icon is returned in hickory form.
              "
              (if (icon-exists? this path)
                (parse (slurp (io/file path)))
                false)))))

(defn dbg [a]
  (println (str "====\n\n" a "\n\n"))
  a)

#?(:cljs (deftype SVGIO []
           SVGIOOperations

          (icon-exists?
            [this c path]
            (go
              (let [r-chan (chan)
                    handler       (fn [r] (put! r-chan {:response r
                                                        :error false}))
                    error-handler (fn [r] (put! r-chan {:response r
                                                        :error true}))]
                (GET path
                     {:handler handler
                      :error-handler error-handler
                      :with-credentials? false
                      :response-format :raw
                      :headers {"Accept" "text/plain, image/svg+xml,text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8"}});"image/svg+xml"

                (let [resp (<! r-chan)]
                  (put! c (not (:error resp)))))))

          (import-icon
            [this c path]
            "
              Import an icon from the given path with the given protocol.
              ie http://path.svg or file:///path.svg  Returns nil if the icon canot be
              loaded.  The icon is returned in hickory form.
            "
            (go
              (let [r-chan (chan)
                    handler (fn [r] (put! r-chan {:response r
                                                  :error false}))
                    error-handler (fn [r] (put! r-chan {:response r
                                                        :error true}))]
                (GET path
                     {:handler handler
                      :error-handler error-handler
                      :with-credentials? false
                      :response-format :raw
                      :headers {"Accept" "text/plain, image/svg+xml,text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8"}});"image/svg+xml"


                (let [resp (<! r-chan)]
                  ;(println (str "import-icon\n=====\n " resp "\n\n"))
                  (if (:error resp)
                    (put! c false)
                    (do
                      (put! c (-> resp
                                  :response
                                  parse-fragment
                                  first
                                  as-hickory))))))))))
                                  ;dbg))))))))))



#?(:cljs (def IO (SVGIO.)))
