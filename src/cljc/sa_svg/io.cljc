(ns sa-svg.io
  "
    Logic required to import, scale and transform icons for project sa.

    Note that we are covering both cljs and clj implementations here.  File
    operations may be valid in java may not be valid in clojurescript.

  ")



(def io-erros (->  (make-hierarchy)
                   (derive ::input-error                     ::io-error)
                   (derive ::file-not-found                  ::input-error)
                   (derive ::protocol-not-found              ::input-error)))



(defprotocol SVGIOOperations
  "
    Protocol describes the operations needed to input and output svg icons for
    project sa.
  "

  #?(:clj
      (icon-exists?
        [this path]))

  #?(:cljs
      (icon-exists?
        [this chan path]))

  #?(:clj
      (import-icon
        [this path]
        "
          Import an icon from the given path with the given protocol.
          ie http://path.svg or file:///path.svg  Returns nil if the icon canot be
          loaded.  The icon is returned in hickory form.
        "))

  #?(:cljs
      (import-icon
        [this chan path]
        "
          Import an icon from the given path with the given protocol.
          ie http://path.svg or file:///path.svg  Returns nil if the icon canot be
          loaded.  The icon is returned in hickory form.
        ")))
