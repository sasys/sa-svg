(ns sa-svg.logic
  "
    Logic required to import, scale and transform icons for project sa.
  ")

(defprotocol SVGLogicOperations
  "
    Protocol describes the operations needed to manipulate svg icons for project
    sa.  An icon must be in Hiccup form.
  "

; SVG element operations
  #?(:clj
      (find-svg-element
        [this hickory-icon]
        "
          Given a hickory element, find the svg element,
          return the svg element as hickory or nil
        "))

  #?(:cljs
      (find-svg-element
        [this hickory-icon]
        "
          Given a hickory element, find the svg element,
          return the svg element as hickory or nil
        "))

  #?(:clj
      (is-svg-element?
        [this hickory-icon]
        "
          Is the given hickory icon an svg element
        "))

  #?(:cljs
      (is-svg-element?
        [this hickory-icon]
        "
          Is the given hickory icon an svg element
        "))

; Enclosing group operations
  #?(:clj
      (find-sa-outer-group
        [this hickory-icon]
        "
          Given a hickory element, find the outer element,
          return the outer group element as hickory or nil
        "))

  #?(:cljs
      (find-sa-outer-group
        [this hickory-icon]
        "
          Given a hickory element, find the outer element,
          return the outer group element as hickory or nil
        "))

  #?(:clj
      (has-sa-outer-group-element?
        [this hickory-icon]
        "
          Does the given icon have an outer group element?
        "))

  #?(:cljs
      (has-sa-outer-group-element?
        [this hickory-icon]
        "
          Does the given icon have an outer group element?
        "))

  #?(:clj
      (add-outer-group-element
        [this hickory-icon]
        "
          Add an outer group to the icon.  If it already has the
          outer group do nothing.
        "))

  #?(:cljs
      (add-outer-group-element
        [this hickory-icon]
        "
          Add an outer group to the icon.  If it already has the
          outer group do nothing.
        "))

;Text (title element) operations
  #?(:clj
      (add-title-text
        [this title-text hickory-icon]
        "
          Add the given title to the icon show that it shows up in the center bottom
          region of the icon.  If no text field is present, create it.
        "))

  #?(:cljs
      (add-title-text
        [this title-text hickory-icon]
        "
          Add the given title to the icon show that it shows up in the center bottom
          region of the icon.  If no text field is present, create it.
        "))


  #?(:clj
      (get-title-text
        [this hickory-icon]
        "
          Get the title from the icon.
        "))

  #?(:cljs
      (get-title-text
        [this hickory-icon]
        "
          Get the title from the icon.
        "))

  #?(:clj
      (find-sa-text-element
        [this hickory-icon]
        "
          a hickory element, find the text element,
          return the text element as hickory or nil
        "))

  #?(:cljs
      (find-sa-text-element
        [this hickory-icon]
        "
          a hickory element, find the text element,
          return the text element as hickory or nil
        "))

  #?(:clj
      (has-sa-text-element?
        [this hickory-icon]
        "
          Does the given icon have a title text field
        "))

  #?(:cljs
      (has-sa-text-element?
        [this hickory-icon]
        "
          Does the given icon have a title text field
        "))

  #?(:clj
      (add-title-element
        [this hickory-icon]
        "
          Add a title text element (empty) to the icon.  If it already has the
          text element do nothing.
        "))

  #?(:cljs
      (add-title-element
        [this hickory-icon]
        "
          Add a title text element (empty) to the icon.  If it already has the
          text element do nothing.
        "))

  #?(:clj
      (import-icon-url
        [this url]
        "
          Import an icon from a url
        "))

  #?(:clj
      (import-icon-file
        [this file-path]
        "
          Import an icon from a file
        "))

  #?(:cljs
      (import-icon-url
        [this c url]
        "
          Import an icon from a url
        "))

  #?(:cljs
      (import-icon-string
        [this svg-string]
        "
          Import an icon from an svg string
        "))

  #?(:clj
      (filter-kvs
        [this hickory-icon]
        "Given a hickory icon remove unecessary attributes"))

  #?(:cljs
      (filter-kvs
        [this hickory-icon]
        "Given a hickory icon remove unecessary attributes"))

  #?(:cljs
      (react-style->hiccup
        [this hickory-icon]
        "Given a hickory icon, fix the style map to use attributes instead of strings"))


  #?(:cljs
      (add-viewport
        [this hickory-icon [width height]]))

  #?(:cljs
      (add-viewbox
        [this hickory-icon [x y width height]])))
