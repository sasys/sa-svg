(ns sa-svg.logic-impl.logic
  (:require  [sa-svg.logic                :as svgl :refer [SVGLogicOperations]]
             [sa-svg.logic-ops            :as lo]
            #?(:cljs [sa-svg.io-impl.io   :refer [IO]])
            #?(:cljs [sa-svg.io           :as svgio])
            #?(:cljs [clojure.core.async  :refer [take! go put! chan]])
            #?(:cljs [hickory.core        :refer [parse-fragment as-hickory]])
            #?(:cljs [hickory.zip         :as hz   :refer [hickory-zip]])
            #?(:cljs [clojure.core.async  :refer [go put! <!]])))

#?(:clj (def LogicOperations
          (reify SVGLogicOperations

            (find-svg-element [this hickory-icon]
              (lo/find-svg-element hickory-icon))

            (is-svg-element? [this hickory-icon]
              (not (false? (svgl/find-svg-element this hickory-icon))))

            (find-sa-outer-group [this hickory-icon]
              (lo/find-sa-outer-group hickory-icon))

            (has-sa-outer-group-element? [this hickory-icon]
              (lo/has-sa-outer-group-element? hickory-icon))

            (add-outer-group-element [this hickory-icon]
              (lo/add-outer-group-element hickory-icon))

            (add-title-text [this title-text hickory-icon]
              (lo/add-title-text title-text hickory-icon))

            (get-title-text [this hickory-icon]
              (lo/get-title))

            (find-sa-text-element [this hickory-icon]
              (lo/find-sa-text-element hickory-icon))

            (has-sa-text-element? [this hickory-icon]
              (lo/has-sa-text-element? hickory-icon))

            (add-title-element [this hickory-icon]
              (lo/add-title-element hickory-icon))

            (import-icon-url [this url]
              ;read url
              ;add required sections
              nil)

            (import-icon-file [this url]
              ;read file
              ;add required sections
              nil))))


#?(:cljs (deftype LogicOperations []
           SVGLogicOperations

           (find-svg-element [this hickory-icon]
             (lo/find-svg-element hickory-icon))

           (is-svg-element? [this hickory-icon]
             (not (false? (lo/find-svg-element hickory-icon))))

           (find-sa-outer-group [this hickory-icon]
             (lo/find-sa-outer-group hickory-icon))

           (has-sa-outer-group-element? [this hickory-icon]
             (lo/has-sa-outer-group-element? hickory-icon))

           (add-outer-group-element [this hickory-icon]
             (lo/add-sa-outer-group-to-node hickory-icon))

           (add-title-text [this title-text hickory-icon]
             (lo/add-title-text title-text hickory-icon))

           (get-title-text [this hickory-icon]
             (lo/get-title hickory-icon))

           (find-sa-text-element [this hickory-icon]
             (lo/find-sa-text-element hickory-icon))

           (has-sa-text-element? [this hickory-icon]
             (lo/has-sa-text-element? hickory-icon))

           (add-title-element [this hickory-icon]
             (let [h (lo/add-title-element hickory-icon)]
               h))

           (import-icon-string [this svg-string]
             (let [imported (-> svg-string
                                parse-fragment
                                first
                                as-hickory)
                   fixed (svgl/add-outer-group-element this imported)]
                (if (true? (svgl/has-sa-text-element? this fixed))
                  fixed
                  false)))


           (import-icon-url [this c url]
             (let [icon-chan (chan)]
               ;icon exists?
               (svgio/icon-exists? IO icon-chan url)
               ;read file
               (go
                  (if (<! icon-chan)
                   (do
                     (svgio/import-icon IO icon-chan url)
                     (let [icon (<! icon-chan)]
                       (if icon
                        ;add required sections
                          (let [fixed (svgl/add-outer-group-element this icon)]
                            (if (true? (svgl/has-sa-text-element? this fixed))
                              (put! c fixed)
                              (put! c false)))
                        (put! c false))))
                   (put! c false)))))


           (filter-kvs [this hickory-icon]
             (lo/filter-kvs (hz/hickory-zip hickory-icon)  lo/edit-remove-attrs lo/default-unwanted-keys))


           (react-style->hiccup
             [this hickory-icon]
             (lo/react-style->hiccup hickory-icon))


           (add-viewport
             [this hickory-icon [width height]]
             (lo/add-viewport hickory-icon [width height]))

           (add-viewbox
             [this hickory-icon [x y width height]]
             (lo/add-view-box hickory-icon [x y width height]))))

#?(:cljs (def Logic (LogicOperations.)))

;
; The first, most outer element should be an svg tag
; The next, first and only child should be a group.  If this is missing, add it.
; If the group had to be added, include a text element
; If the group had to be added, add the rest of the svg to it.
; Then reprocess it.
; If the element has a group as its first and only child, it should also contain a text
; element, it it is missing, add it.  Then reprocess.
; The group should contain another group element with a transform tag.  If it is
; missing, add it.
