(ns sa-svg.logic-ops
  (:require        [clojure.zip             :as cz]
                   [hickory.zip             :as hz   :refer [hickory-zip]]
                   [hickory.select          :as hs]
                   [hickory.core            :refer [parse
                                                    parse-fragment
                                                    as-hiccup
                                                    as-hickory]]
                   [clojure.string          :as st]
                   [clojure.walk            :as w]))
                   ;[cljs.pprint             :as pp]))


(def sa-name-attribute-name  "sa:name")
(def sa-svg-text-class       "sa:title")
(def sa-svg-text-title-id    "sa:title")
(def sa-svg-st-icon-group-id "sa:st-icon-group")
(def sa-svg-icon-group-id    "sa:icon-group")

;This is the list of keys we might find in some hiccup derived from
;an external source (perhaps created by inkscape)
(def default-unwanted-keys [:inkscape:groupmode
                            :inkscape:label
                            :xmlns:inkscape
                            :xmlns:dc
                            :xmlns:sodipodi
                            :sodipodi:role
                            :xmlns:cc
                            :inkscape:connector-curvature
                            :inkscape:pageshadow
                            :fit-margin-right
                            :sodipodi:nodetypes
                            :sodipodi:docname
                            :xml:space
                            :inkscape:zoom
                            :inkscape:window-maximized
                            :inkscape:current-layer
                            :inkscape:exportXdpi
                            :inkscape:exportYdpi
                            :inkscape:exportxdpi
                            :inkscape:exportydpi
                            :inkscape:cx
                            :inkscape:cy
                            :inkscape:window-width
                            :inkscape:window-height
                            :inkscape:document-units
                            :inkscape:window-x
                            :inkscape:pageopacity
                            :inkscape:version
                            :rdf:resource
                            :rdf:about])







(def sa-st-icon-group
  {:type :element,
   :attrs {:sa:id "sa:st-icon-group",
           :transform "translate(0, 0) scale(2.5, 2.5)"},
   :tag :g,
   :content []})

(def sa-text
  {:type :element
   :tag :text
   :attrs {:class (str sa-svg-text-class " icon local sbnl pod")
           :sa:id sa-svg-text-title-id
           :id "title"
           :y "26.794041"
           :x "7"
           :text-anchor "middle"
           :xml:space "preserve"
           :font-size "4px"}
   :content nil})

(def sa-icon-group
 {:type :element
  :tag :g
  :attrs {:sa:id sa-svg-icon-group-id :transform "translate(0, 0)" :id "layer1"}
  :content [sa-text]})

(def sa-svg
  {:type  :element
   :tag   :svg
   :attrs {:viewBox "0 0 700 600"}
   :width "200"
   :height "200"
   :content []})


(defn iter-zip
  "
    Iterate over all the nodes
  "
  [zipper]
  (->> zipper
       (iterate cz/next)
       (take-while (complement cz/end?))))

(defn add-sa-outer-group-to-node
  "
    Add the standard sa outer group, should be first child of the svg element
  "
  [hickory-svg-element]
  (if (vector? (:content hickory-svg-element))
    (->> [(assoc sa-icon-group :content (cons (assoc sa-st-icon-group :content (into (:content sa-st-icon-group)
                                                                                     (:content hickory-svg-element)))
                                              (:content sa-icon-group)))]
         (assoc hickory-svg-element :content))
    hickory-svg-element))


(def el {:type :element,
         :attrs
           {:sa:id "sa:st-icon-group",
            :transform "translate(0, 0) scale(2.5, 2.5)",}
         :tag :g,
         :content nil})

; (defn prt [e new]
;   (println (str "\n====NODE====\n"))
;   (pp/pprint e)
;   (println (str "\n\n"))
;   (println new)
;   (println (str "\n\n")))

(defn add-transform-string-to-node
  "
    Given a hickory zip node, change the translate and scale string.
  "
  [hickory-zip-element new-trans-str]
  ;(prt hickory-zip-element new-trans-str)
  (if (or (not (map? hickory-zip-element)) (vector? hickory-zip-element))
      (->  hickory-zip-element
           first
           (assoc-in [:attrs :transform] new-trans-str))
      (assoc-in hickory-zip-element [:attrs :transform] new-trans-str)))



(defn add-sa-text-to-node
  "
    Editor to be used in a zip on a text element to add or clear a text label
  "
  ([n]
   (let [ntext (assoc sa-text :content "")]
     (if (vector?  n)
       [ntext]
       ntext)))
  ([n t]
   (let [ntext (assoc sa-text :content t)]
     (if (vector?  n)
       [ntext]
       ntext))))

(defn find-svg-element
  [hickory-icon]
  (let [svg (hs/select (hs/child (hs/tag :svg)) hickory-icon)]
    (if (seq? (seq svg))
      (first svg)
      false)))

(defn find-sa-outer-group
  [hickory-icon]
  (let [e (hs/select  (hs/child
                        (hs/and (hs/tag  :g)
                                (hs/attr :sa:id #(= % sa-svg-icon-group-id))))
                      hickory-icon)]
    (if (seq? (seq e))
      (first e)
      false)))

(defn has-sa-outer-group-element?
  [hickory-icon]
  (not (false? (find-sa-outer-group hickory-icon))))


(defn add-outer-group-element
  [hickory-icon]
  (-> (find-svg-element hickory-icon)
      hz/hickory-zip
      (cz/edit add-sa-outer-group-to-node)
      cz/node))

(defn add-title-text
  [title-text hickory-icon]
  (-> hickory-icon
      hickory-zip
      cz/down
      cz/down
      cz/right
      (cz/edit #(add-sa-text-to-node % title-text))
      cz/root))

(defn sa-text-element?
  "
    Is the given node an sa title node
  "
  [hickory-node]
  (some-> hickory-node :attrs :sa:id (= "sa:title")))


(defn find-sa-text-element
  [hickory-icon]
  (let [nodes (-> hickory-icon
                  hickory-zip
                  iter-zip)

        text-node (first (filter #(sa-text-element? (first %)) nodes))]
    (if (nil? text-node)
      false
      text-node)))

(defn has-sa-text-element?
  [hickory-icon]
  (not (false? (find-sa-text-element hickory-icon))))

(defn add-title-element
  [hickory-icon]
  (-> (find-svg-element hickory-icon)
      hz/hickory-zip
      (cz/edit add-sa-text-to-node)
      cz/node))


(defn get-title
  [hickory-icon]
  (if (has-sa-text-element? hickory-icon)
    (:content (first (find-sa-text-element hickory-icon)))
    false))



(defn update-transform-string
  "
    Given a group transform string update the translate and the scale values
  "
  [transform-string [[tx ty] [sx sy]]]
  (str "translate(" tx ", "ty") scale("sx", "sy")"))


(defn make-transform-string
  "
      Make a new transform string from the given values
    "
  [[[tx ty] [sx sy]]]
  (str "translate(" tx ", "ty") scale("sx", "sy")"))

(defn get-transform-string
  [hickory-icon]
  (let [grp (find-sa-outer-group hickory-icon)]
    ; (println (str "OUTTER GROUP \n\n"))
    ; (cljs.pprint/pprint grp)
    ; (println (str "=============\n\n"))
    (if grp
      (-> grp
          :content
          first
          :attrs
          :transform)
      false)))

(defn get-group-transform
  [transform-string]
  (let [trans-strs (filter #(not (st/blank? %)) (re-seq #"\d*\.\d*|\d*" transform-string))]
    (if (= 4 (count trans-strs))
      (partition 2 (map (fn [s] (if (st/includes? s ".")
                                  #?(:clj  (Float/parseFloat s))
                                  #?(:cljs (js/parseFloat s))
                                  #?(:clj  (Integer/parseInt s))
                                  #?(:cljs (js/parseInt s)))) trans-strs)))))


(defn translate-x
  "
    Set the x translate
  "
  [hickory-icon x]
  hickory-icon)

(defn translate-y
  "
    Set the y translate
  "
  [hickory-icon y]
  hickory-icon)

(defn scale-x
  "
    Set the x scale
  "
  [hickory-icon x]
  hickory-icon)

(defn scale-y
  "
    Set the y scale
  "
  [hickory-icon y]
  hickory-icon)

(defn update-group-transform
  [hickory-icon [[trans-x trans-y] [scale-x scale-y]]]
  (let [new-str (make-transform-string [[trans-x trans-y] [scale-x scale-y]])]
    (-> hickory-icon
        hickory-zip
        cz/down
        cz/down
        (cz/edit #(add-transform-string-to-node % new-str))
        cz/root)))

(defn edit-remove-attrs
  "Removes the specified list of attributes from the given node"
  [node ks]
  (assoc node :attrs (apply dissoc (:attrs node) ks)))

(defn filter-kvs
  "Given a hickory zipper remove any key attributes where the key matches any of the ks
   Leaves the loc at :end.
   You need to make a new zipper from this to do other processing
  "
  [hickory-zipper f ks]
  (loop [n hickory-zipper]
    (if (or (nil? n) (cz/end? n))
      n
      (let [node (cz/node n)]
        (if (not= (:attrs node) nil)
          (recur (cz/next (cz/edit n f ks)))
          (recur (cz/next n)))))))

(defn string->tokens
  "Takes a string with styles and parses it into properties and value tokens.
   Turns \"color:red;background:black; font-style: normal    ;font-size : 20px\" into
   (\"color\" \"red\" \"background\" \"black\" \"font-style\" \"normal\" \"font-size\" \"20px\")
  "
  [style]
  {:pre [(string? style)]
   :post [(even? (count %))]}
  (->> (st/split style #";")
       (mapcat #(st/split % #":"))
       (map st/trim)))

(defn tokens->string
  "
      Takes a map of keys and values and makes a style string.
      EG turns {\"color\" \"red\" \"background\" \"black\"}
      into \"color:red;background:black\"
  "
  [style-map]
  (apply str (interpose ";" (map #(str %1 ":" %2)  (map name (keys style-map)) (vals style-map)))))



(defn tokens->map
  "Takes a seq of tokens with the properties (even) and their values (odd)
    and returns a map of {properties values}"
  [tokens]
  {:pre [(even? (count tokens))]
   :post [(map? %)]}
  (zipmap (keep-indexed #(if (even? %1) %2) tokens)
          (keep-indexed #(if (odd? %1) %2) tokens)))


(defn style->map
  "Takes an inline style attribute string and converts it to a React Style map"
  [style]
  (tokens->map (string->tokens style)))


(defn react-style->hiccup
  "
    Transforms a style inline attribute into a style map for React
    See https://stackoverflow.com/questions/28257750/how-to-convert-html-tag-with-style-to-hiccup-react-problems
    Converts a form like {:style \"a:b;c:d\"} into {:style {:a \"b\" :c \"d\"}}
  "
  [coll]
  (w/postwalk
   (fn [x]
     (if (and (map? x) (> (count (keys x)) 0) (some #{:style} (keys x)))
       (update-in x [:style] style->map)
       x))
   coll))


(defn has-svg-element
  "
    True => svg element is present
  "
  [hickory-icon]
  (not (false? (find-svg-element hickory-icon))))


(defn add-svg-element
  "
    Wraps the hickory icon in an svg element
  "
  [hickory-icon]
  (if (vector? hickory-icon)
    (assoc sa-svg :content [(first hickory-icon)])
    (assoc sa-svg :content [hickory-icon])))


(defn get-view-box
  "
    Get the view box specifications or false
  "
  [hickory-icon]
  (let [view-box  (-> hickory-icon
                      find-svg-element
                      :attrs
                      :viewbox)]
    (if (nil? view-box)
        false
        (map (fn [%1] (when (re-matches #"^\d+$" %1)
                        #?(:clj  (Integer/parseInt %1))
                        #?(:cljs (js/parseInt %1))))

            (st/split view-box #" ")))))


(defn has-view-box
  "
    Has the svg element got a view box
  "
  [hickory-icon]
  (not (false? (get-view-box hickory-icon))))


(defn add-view-box
  "
    Add the given view box to the element if there is none, or replace the
    existing view box with the supplied view box.
  "
  [hickory-icon [x y width height]]
  (let [svg (find-svg-element hickory-icon)]
    (assoc-in svg [:attrs :viewbox] (str x " " y " " width " "height))))


(defn update-viewbox-x
  "
    Update the x position of the viewbox
  "
  [hickory-icon new-x]
  (let [[_ y width height] (get-view-box hickory-icon)]
    (add-view-box hickory-icon [new-x y width height])))


(defn update-viewbox-y
  "
      Update the y position of the viewbox
  "
  [hickory-icon new-y]
  (let [[x _ width height] (get-view-box hickory-icon)]
    (add-view-box hickory-icon [x new-y width height])))


(defn update-viewbox-width
  "
    Update the width of the viewbox
  "
  [hickory-icon new-width]
  (let [[x y _ height] (get-view-box hickory-icon)]
    (add-view-box hickory-icon [x y new-width height])))


(defn update-viewbox-height
  "
    Update the height of the viewbox
  "
  [hickory-icon new-height]
  (let [[x y width _] (get-view-box hickory-icon)]
    (add-view-box hickory-icon [x y width new-height])))

(defn add-viewport
  "
    Given a hickory icon, add (or replace) the width and height values (viewport).
  "
  [hickory-svg-icon [width height]]
  (let [svg (find-svg-element hickory-svg-icon)]
    (-> svg
        (assoc-in [:attrs :width]  width)
        (assoc-in [:attrs :height] height))))

(defn get-viewport
  "
    Get the viewport from the given hickory svg icon.
  "
  [hickory-svg-icon]
  (let [[width height]  [(get-in hickory-svg-icon [:attrs :width])
                         (get-in hickory-svg-icon [:attrs :width])]]
    (if (and (not (nil? width)) (not (nil? height)))
      [width height]
      false)))

(defn has-viewport?
  "
    Does he given hickory svg icon have a viewport.
  "
  [hickory-svg-icon]
  (not (false? (get-viewport hickory-svg-icon))))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Model operation of add-sa-text-to-node

(def circle "<svg
                xmlns:svg=\"http://www.w3.org/2000/svg\"
                xmlns=\"http://www.w3.org/2000/svg\">
                <ellipse
                 style=\"fill:#ffffff;stroke:#000000;stroke-width:2px;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0.99298\"
                 id=\"path1537\"
                 cx=\"24.003242\"
                 cy=\"20.85918\"
                 rx=\"17.71895\"
                 ry=\"16.358004\" />
             </svg>")

(def circle1 "<svg
               xmlns:svg=\"http://www.w3.org/2000/svg\"
               xmlns=\"http://www.w3.org/2000/svg\">
               Hello
             </svg>")


;Model operation of add-title-text
(defn explr1 []
  (let [hickory-circ (-> circle
                         parse-fragment
                         first
                         as-hickory)
        circ  (add-sa-outer-group-to-node hickory-circ)
        xmlcirc (add-title-text "New Text" circ)]
    xmlcirc))

;Model operation of iter-zip
(iter-zip (hickory-zip (explr1)))

;model operation of find
(find-sa-text-element (explr1))

;model operation of has
(has-sa-text-element? (explr1))


(get-title (explr1))
