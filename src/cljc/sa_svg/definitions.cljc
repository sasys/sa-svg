(ns sa-svg.definitions)

;These are the ids that we expect every svg element to have in its identifier
;ie "sa:source", "sa:title" "sa:name" as the keywords to identify a particular
;icon set and name as attributes
; (def namespace "sa")
; (def source    "source")
; (def name      "name")
; (def title     "title")

;outer group with id like
;"sa:id" "sa:icon-group"

;identify the title text element as "sa:title"
;"sa:id" "sa:title"

;The text element class will be


;Sample simple icon before ingestion process

;<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
;  <circle cx="50" cy="50" r="50"/>)
;</svg>

;After ingestion process
;<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
;  <g transform="translate(0, 0) scale(2.5, 2.5)")
;    <circle cx="50" cy="50" r="50"/>
;    <text class="icon local sbnl pod"
;          id="title"
;          y="26.794041"
;          x="7"
;          text-anchor="middle"
;          xml:space="preserve"
;          font-size="4px">
;    </text>

;</svg>
