(ns sa-svg.main)
;
; Unused main, for trial puposes only, will be removed.
;
;


(enable-console-print!)

(def circ-missing-svg "<circle cx=\"50\" cy=\"50\" r=\"50\"/>")

(def circ-missing-outer-group
  "<svg viewBox=\"0 0 100 100\" xmlns=\"http://www.w3.org/2000/svg\" sa:name=\"after-circle\">
    <circle cx=\"50\" cy=\"50\" r=\"50\">
   </svg>")


(def after-circle
  "<svg viewBox=\"0 0 100 100\" xmlns=\"http://www.w3.org/2000/svg\" sa:name=\"after-circle\">
    <g sa:id=\"sa:icon-group\">
      <g sa:id=\"sa:st-icon-group\"
         transform=\"translate(0, 0) scale(2.5, 2.5)\">
        <circle cx=\"50\" cy=\"50\" r=\"50\"/>
      </g>
      <text class=\"sa:title icon local sbnl pod\"
            sa:id=\"sa:title\"
            id=\"title\"
            y=\"26.794041\"
            x=\"7\"
            text-anchor=\"middle\"
            xml:space=\"preserve\"
            font-size=\"4px\">
      </text>
    </g>
  </svg>")


(defn ^:export init []
  (.log js/console "\nMain Running\n============\n"))
  ; (.log js/console (str "find-svg circ-missing: " (svgl/find-svg-element lg/Logic (hc/as-hickory (hc/parse  circ-missing-svg)))))
  ; (.log js/console (str "is-svg-element? circ-missing: " (svgl/is-svg-element? lg/Logic (hc/as-hickory (hc/parse  circ-missing-svg)))))
  ; (.log js/console (str "find-svg after-circle: " (svgl/find-svg-element lg/Logic (hc/as-hickory (hc/parse  after-circle)))))
  ; (.log js/console (str "is-svg-element? after-circle: " (svgl/is-svg-element? lg/Logic (hc/as-hickory (hc/parse  after-circle)))))
  ; (.log js/console (str "has-outer-group? circ-missing-outer-group: " (svgl/has-sa-outer-group-element? lg/Logic (hc/as-hickory (hc/parse circ-missing-outer-group)))))
  ; (let [s (svgl/add-outer-group-element lg/Logic (hc/as-hickory (hc/parse  circ-missing-outer-group)))]
  ;   (.log js/console (str "has-outer-group? circ-missing-outer-group after add: " (svgl/has-sa-outer-group-element? lg/Logic s)))
  ;   (.log js/console (str "is-svg-element? add outer group: " (svgl/is-svg-element? lg/Logic s)))))
