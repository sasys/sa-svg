
[![CircleCI](https://circleci.com/bb/sasys/sa-svg.svg?style=svg)](https://circleci.com/bb/sasys/sa-svg)

Provides SVG support the sa-frontend project.  Enables:

* import
* preparation
* scaling
* transformation
* title addition
* title edit

The intention is to allow any svg icon to imported and used in the model and the
markdown.

## Repl

Run a local repl in the root of the project
```
 clj -M:nRepl
```

Then connect to it using proto-repl in atom.

## Build

```
  clj -X:dev-build
```

## Lint

```
  ./lint.sh
```

## Test clj

__NOTE THE -X__
```
  clj -X:test-clj
```

## Test cljs

```
  clj -M:test-cljs
```

## Local circleci

```
  circleci local execute --job build
```

### Notes

See https://webdesign.tutsplus.com/tutorials/svg-viewport-and-viewbox-for-beginners--cms-30844 for
more information on viewPort and viewBox.


